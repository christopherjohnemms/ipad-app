//
//  LogInViewController.m
//  TATAipad
//
//  Created by TAMBA Internet on 27/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "TambaTabController.h"
#import "LogoViewController.h"
#import "HomeViewController.h"
#import "NotesViewController.h"
#import "NavigationViewController.h"
#import "SyncViewController.h"
#import "LogInViewController.h"


@interface LogInViewController ()

@end

@implementation LogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// --------------------------------------------------------------------------------

#pragma mark -
#pragma mark UI Event methods
- (IBAction)loginButtonDidTouchUpInside:(UIButton *)sender
{
    /*
     
     When app goes home, it will show a modal box to ask if user is finished with the session
     
     if finished
     {
        go back to login screen and reset session
     }
     else
     {
        close window and go back to the session
     }
     
     
     */
    
    
    // Create new tabBarController to contain app
    TambaTabController *tabBarController = [TambaTabController new];

    // Tabs
    UINavigationController *navigationController1 = [[UINavigationController alloc] initWithRootViewController:[HomeViewController new]]; // Home
    UINavigationController *navigationController2 = [[UINavigationController alloc] initWithRootViewController:[NotesViewController new]]; // Notes
    UINavigationController *navigationController3 = [[UINavigationController alloc] initWithRootViewController:[NavigationViewController new]]; // Navigation
    UINavigationController *navigationController4 = [[UINavigationController alloc] initWithRootViewController:[SyncViewController new]]; // Sync

    [tabBarController setViewControllers:@[navigationController1, navigationController2, navigationController3, navigationController4]];
    
    
    
    [(UIViewController *)[tabBarController.viewControllers objectAtIndex:0] setTitle:@"Home"];
    [(UIViewController *)[tabBarController.viewControllers objectAtIndex:1] setTitle:@"Notes"];
    [(UIViewController *)[tabBarController.viewControllers objectAtIndex:2] setTitle:@"Navigation"];
    [(UIViewController *)[tabBarController.viewControllers objectAtIndex:3] setTitle:@"Sync"];
    

    [[UIApplication sharedApplication].delegate window].rootViewController = tabBarController;
    
}




@end
