//
//  PersonEntryViewController.m
//  TATAipad
//
//  Created by TAMBA Internet on 28/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "HomeViewController.h"
#import "PersonEntryViewController.h"


@interface PersonEntryViewController ()

@end

@implementation PersonEntryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissQuestion:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
