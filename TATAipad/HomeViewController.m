//
//  HomeViewController.m
//  TATAipad
//
//  Created by TAMBA Internet on 28/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "PersonEntryViewController.h"
#import "MarketsViewController.h"
#import "HomeViewController.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.title = @"Home";
    
    /*
     If first time 
     {
        Show up a form for data entry of person
        
        If user chooses bypass form
        {
            Close form
        }
     
     }
     else if not first time
     {
        Dont show up form
     }
     */
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        PersonEntryViewController *personVC = [PersonEntryViewController new];
        [self presentViewController:personVC animated:YES completion:nil];
    });
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)marketsButtonDidTouchUpInside:(id)sender
{
    MarketsViewController *marketsVC = [MarketsViewController new];
    [self.navigationController pushViewController:marketsVC animated:YES];
}

@end
