//
//  IntroViewController.m
//  TATAipad
//
//  Created by TAMBA Internet on 27/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "IntroViewController.h"
#import "LogInViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
     
     Load some sort of animation here
     Create a button to skip if needs-be
     
    */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)introToLoginButton:(id)sender
{
    LogInViewController *loginVC = [LogInViewController new];
    [self.navigationController pushViewController:loginVC animated:YES];
    
}

@end
