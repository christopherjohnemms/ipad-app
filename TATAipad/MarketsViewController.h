//
//  MarketsViewController.h
//  TATAipad
//
//  Created by TAMBA Internet on 28/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface MarketsViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong,nonatomic) NSMutableArray *pictures;
@property (strong,nonatomic) NSMutableArray *descriptions;
@property (nonatomic) BOOL wrap;

@end
