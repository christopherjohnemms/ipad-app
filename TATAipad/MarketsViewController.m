//
//  MarketsViewController.m
//  TATAipad
//
//  Created by TAMBA Internet on 28/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "MarketsViewController.h"


@implementation MarketsViewController
@synthesize pictures,descriptions,wrap,carousel,label;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //set up carousel data
        wrap = YES;
        self.pictures = [NSMutableArray arrayWithObjects:@"Test1.png",
                        @"Test2.png",
                        @"Test3.png",
                        @"Test4.png",
                        @"Test5.png",
                        @"Test6.png",
                        @"Test7.png",
                        nil];
        self.descriptions = [NSMutableArray arrayWithObjects:@"This is the first slide",
                             @"This is the second slide",
                             @"This is the third slide",
                             @"This is the fourth slide",
                             @"This is the fifth slide",
                             @"This is the sixth slide",
                             @"This is the seventh slide",
                             nil];
    }
    return self;
}

- (void)viewDidLoad
{
    carousel.type = iCarouselTypeLinear;
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    // Load List of Data for Markets
    // Automotive
    // Construction
    // Defence and Security
    // Energy & Power
    // Lifting and Excavating
    // Packaging
    // Rail
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [pictures count];
}

- (NSUInteger)numberOfVisibleItemsInCarousel:(iCarousel *)carousel
{
    //limit the number of items views loaded concurrently (for performance reasons)
    return 7;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    //create a numbered view
	view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[pictures objectAtIndex:index]]];
	return view;
}

- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
	//note: placeholder views are only displayed on some carousels if wrapping is disabled
	return 0;
}


- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    //usually this should be slightly wider than the item views
    return 1024;
}


- (BOOL)carouselShouldWrap:(iCarousel *)carousel
{
    //wrap all carousels
    return wrap;
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)aCarousel
{
    [label setText:[NSString stringWithFormat:@"%@", [descriptions objectAtIndex:aCarousel.currentItemIndex]]];
}




@end
