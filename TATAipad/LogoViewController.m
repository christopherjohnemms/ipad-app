//
//  LogoViewController.m
//  TATAipad
//
//  Created by TAMBA Internet on 27/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "LogoViewController.h"
#import "IntroViewController.h"


@interface LogoViewController ()

@end



@implementation LogoViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)homeToIntroButton:(id)sender
{
    
    //Create a new object of the LoginViewController first
    //Initialize it by creating a new LogInViewController
    //so loginVC = (an instance of) LogInController View
    
    IntroViewController *introVC = [IntroViewController new];
    
    // Load current object with the navigation controller
    // Push the viewController up to the screen
    // No need for pointer *
    
    [self.navigationController pushViewController: introVC animated:YES];
    
    
    

}

@end
