//
//  AppDelegate.h
//  TATAipad
//
//  Created by TAMBA Internet on 27/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
