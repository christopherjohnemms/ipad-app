//
//  TambaTabController.m
//  TATAipad
//
//  Created by TAMBA Internet on 29/11/2013.
//  Copyright (c) 2013 TAMBA Internet. All rights reserved.
//

#import "TambaTabController.h"

@interface TambaTabController () <UITabBarControllerDelegate>

@end

@implementation TambaTabController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.delegate = self;
    NSLog(@"Test");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"%lu", (long)self.selectedIndex);
    UINavigationController *currentNavController = (UINavigationController*)self.selectedViewController;
    [currentNavController popToRootViewControllerAnimated:NO];
}

@end
